# Disaster Response Pipeline Project

This project aims to create a text classifier that helps label 
messages sent during disaster. The classes are a number of 
important elements in disaster response, e.g., water, food. 
The hope is that this information could 
be useful for disaster response teams.


# Running the program

## 1. Run ELT pipeline

To run the ELT pipleline, which prepares the data and dumps in 
into a database, run

```python
python data/process_data.py data/disaster_messages.csv data/disaster_categories.csv data/DisasterResponse.db
```
## 2. Run the ML pipeline

In this step the pre-processed data is used to train a machine
learning model. Run

```python
python models/train_classifier.py data/DisasterResponse.db models/classifier.pkl
```

## 3. Starting The Web App

In the app's directory run
```python
python run.py
```
## Viewing the app

To view the app go to http://0.0.0.0:3001/

## Folder Structure

1. The ELT pipeline and processed data is contained in `data/`

2. The saved model and ml pipeline is contained in `models/`.

3. The app runner and html code is contained in `app/`
