import sys
import pandas as pd
import re
import nltk
from sqlalchemy import create_engine
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.multioutput import MultiOutputClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
import pickle

nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')

stop_words = stopwords.words("english")
lemmatizer = WordNetLemmatizer()

# name of input features, col with messages.
X_COLUMNS = 'message'

# Note, 'child_alone' has been removed from this list 
# because there are not instances of this occuring 
# in the dataset, all it's elements are 0
# 
CAT_NAMES = ['related', 'request', 'offer',
               'aid_related', 'medical_help', 'medical_products', 'search_and_rescue',
               'security', 'military', 'water', 'food', 'shelter',
               'clothing', 'money', 'missing_people', 'refugees', 'death', 'other_aid',
               'infrastructure_related', 'transport', 'buildings', 'electricity',
               'tools', 'hospitals', 'shops', 'aid_centers', 'other_infrastructure',
               'weather_related', 'floods', 'storm', 'fire', 'earthquake', 'cold',
               'other_weather', 'direct_report']


def load_data(database_filepath):
    """
    Load the data from the database `database_filepath` and 
    split into feature 'X' and targets `y`
    
    # Parameters
       database_filepath, str, full path to database
    
    # Returns
        tuple, (X, y, category_names)
          X: numpy array of features data
          y: numpy array of target columns 
          Y_COLUMNS: array of catagory names
          
    """
    
    print('Reading data from ', database_filepath)
    # load data from database
    engine = create_engine('sqlite:///' + database_filepath)
    df = pd.read_sql(sql="SELECT * from messages", con=engine)
    X = df[X_COLUMNS].values
    y = df[CAT_NAMES].values
    
    return X, y, CAT_NAMES


def tokenize(text):
    """
    Tokenize the input text.
    
    # Parameters
        text : str
    # Returns
        list, list of tokens
    
    """
    # lower the case
    text = text.lower()
    # remove the punctuation
    text = re.sub(r"[^a-zA-Z0-9]", " ", text)
    # split into tokens.
    tokens = word_tokenize(text)
    # Remove stop words.
    tokens = [tok for tok in tokens if tok not in stop_words]
    # lemmitize
    [lemmatizer.lemmatize(tok) for tok in tokens]
    return tokens


def build_model():
    """
    Create a pipeline to train the model. This pipeline has 
    two steps a) tokenize and extract a tf-idf matrix from the
    data and b) fit a logistic regression classifier, 1 for each 
    input class. 
    
    # Returns
     sklearn pipeline instance.
    
    """
    # base model is logistic regression
    lr = LogisticRegression()

    # Make pipeline 
    # Note, the adopted hyper-parameters we tunned using 
    # grid search cross validation.
    pipeline = Pipeline([
        ('tf-idf', TfidfVectorizer(tokenizer=tokenize, max_features=5000)),
        ('classifier', MultiOutputClassifier(lr))
    ])

    # Create parameter grid to search.
    parameters = {
        'tf-idf__max_features': [5000, 10000, 20000]
    }

    # Create grid search pipeline.
    cv = GridSearchCV(pipeline, param_grid=parameters)

    return cv


def evaluate_model(model, X_test, Y_test, category_names):
    """
    Evaluation various accuracy statistics on the trained model.
    The accuracy is assesed for each class seperately. 
    
    # Parameters
        model, trained sklearn multiclass model
        X_test, np array of testing data
        Y_test, np array for testing labels
        category_names, list of all the catagories names
    
    # Returns 
        None
    
    """
    
    # Create prediction for the text dataset.
    y_pred = model.predict(X_test)
    
    # Test the accuracy of each estimator 
    # individually, i.e, for each class. 
    for idx, class_name in enumerate(category_names):

        class_predictions = y_pred[:, idx]
        class_labels = Y_test[:, idx]
        report = classification_report(y_true=class_labels, y_pred=class_predictions)

        print('#'*20)
        print('Report for class = {}'.format(class_name))
        print(report)
    
    return None


def save_model(model, model_filepath):
    """
    Save the trained model `model` using pickle
    to the file path 'model_filepath'.

    # Parameters
        model, sklearn trained model
        model_filepath, str, full path to save model to.

    """
    pickle.dump(model, open(model_filepath, 'wb'))


def main():
    if len(sys.argv) == 3:
        database_filepath, model_filepath = sys.argv[1:]
        print('Loading data...\n    DATABASE: {}'.format(database_filepath))
        X, Y, category_names = load_data(database_filepath)
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)

        print('Building model...')
        model = build_model()

        print('Training model...')
        model.fit(X_train, Y_train)

        print('Evaluating model...')
        evaluate_model(model, X_test, Y_test, category_names)

        print('Saving model...\n    MODEL: {}'.format(model_filepath))
        save_model(model, model_filepath)

        print('Trained model saved!')

    else:
        print('Please provide the filepath of the disaster messages database '\
              'as the first argument and the filepath of the pickle file to '\
              'save the model to as the second argument. \n\nExample: python '\
              'train_classifier.py ../data/DisasterResponse.db classifier.pkl')


if __name__ == '__main__':
    main()
